from __future__ import print_function

import numpy as np
# import requests
import tensorflow as tf

from grpc.beta import implementations
from PIL import Image
# from StringIO import StringIO
from tensorflow_serving.apis import predict_pb2
from tensorflow_serving.apis import prediction_service_pb2

# server = '172.21.0.2:9000'
server = 'localhost:9500'
host, port = server.split(':')

#load image
img = Image.open('MNIST-data/mnistasjpg/testSample/img_1.jpg')
img_np = np.array(img)/255.0
img_np = img_np.reshape(1, 784)

#create the RPC stub
channel = implementations.insecure_channel(host, int(port))
stub = prediction_service_pb2.beta_create_PredictionService_stub(channel)

#create the request object and set the name and signature name params
request = predict_pb2.PredictRequest()
request.model_spec.name = 'mnist_prediction_model'
request.model_spec.signature_name = 'predictionz'

#fill in the request object with the necessary data
# CopyFrom(
#   tf.contrib.util.make_tensor_proto(image.astype(dtype=np.float32), shape=[1, height, width, 3]))

request.inputs['x'].CopyFrom(
    tf.contrib.util.make_tensor_proto(img_np, shape=[1, 784], dtype=tf.float32)
)
result_future = stub.Predict(request, 30.)

#get the result
# class_prediction = result_future.outputs['prediction']['classes']
# prob_prediciton = result_future.outputs['prediction']['probabilities']

print(result_future.outputs['classes'].int64_val[0])
# print(prob_prediciton)
