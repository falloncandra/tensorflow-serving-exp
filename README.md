1. Download docker tensorflow-serving image:
	docker pull bitnami/tensorflow-serving:latest 

	or you can follow this tutorial:
		https://hub.docker.com/r/bitnami/tensorflow-serving/

2. Run mnist_model.py to create model and save it into protobuf format
3. Run docker image to serve the model (run this command in the directory that contains docker-compose.yml)
	docker-compose up -d

4. Run mnist_client.py to make a request to the model